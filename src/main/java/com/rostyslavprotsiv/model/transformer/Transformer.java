package com.rostyslavprotsiv.model.transformer;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.Table;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet rs)
            throws SQLException {
        //create new object
        Object entity = null;
        String field;
        try {
            entity = clazz.newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                for (Field f: clazz.getDeclaredFields()) {
                    Column column = f.getAnnotation(Column.class);
                    if (column != null) {
                        field = column.name();
                        f.setAccessible(true);
                        rs.get
                    }
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return entity;
    }
}

