package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

@Table(name = "another_bank_info")
public class AnotherBankInfoEntity {
    @PrimaryKey
    @Column(name = "name", length = 32)
    private String name;
    @Column(name = "founder", length = 64)
    private String founder;

    public AnotherBankInfoEntity() {
    }

    public AnotherBankInfoEntity(String name, String founder) {
        this.name = name;
        this.founder = founder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    @Override
    public String toString() {
        return "AnotherBankInfoEntity{" +
                "name='" + name + '\'' +
                ", founder='" + founder + '\'' +
                '}';
    }
}
