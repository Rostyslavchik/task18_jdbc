package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

import java.math.BigDecimal;
import java.sql.Date;

@Table(name = "operation")
public class OperationEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "date")
    private Date date;
    @Column(name = "type")
    private OperationType type;
    @Column(name = "dollar_amount")
    private BigDecimal dollarAmount;
    @Column(name = "account_id")
    private int accountId;
    @Column(name = "another_bank_account_id")
    private int anotherBankAccountId;

    public OperationEntity() {
    }

    public OperationEntity(int id, Date date, OperationType type,
                           BigDecimal dollarAmount, int accountId,
                           int anotherBankAccountId) {
        this.id = id;
        this.date = date;
        this.type = type;
        this.dollarAmount = dollarAmount;
        this.accountId = accountId;
        this.anotherBankAccountId = anotherBankAccountId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public BigDecimal getDollarAmount() {
        return dollarAmount;
    }

    public void setDollarAmount(BigDecimal dollarAmount) {
        this.dollarAmount = dollarAmount;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAnotherBankAccountId() {
        return anotherBankAccountId;
    }

    public void setAnotherBankAccountId(int anotherBankAccountId) {
        this.anotherBankAccountId = anotherBankAccountId;
    }

    @Override
    public String toString() {
        return "OperationEntity{" +
                "id=" + id +
                ", date=" + date +
                ", type=" + type +
                ", dollarAmount=" + dollarAmount +
                ", accountId=" + accountId +
                ", anotherBankAccountId=" + anotherBankAccountId +
                '}';
    }
}
