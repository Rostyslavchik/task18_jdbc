package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

import java.sql.Date;

@Table(name = "client")
public class ClientEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 64)
    private String name;
    @Column(name = "surname", length = 64)
    private String surname;
    @Column(name = "city", length = 64)
    private String city;
    @Column(name = "birthday")
    private Date birthday;
    @Column(name = "photo_link", length = 64)
    private String photoLink;

    public ClientEntity() {
    }

    public ClientEntity(int id, String name, String surname,
                        String city, Date birthday, String photoLink) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.birthday = birthday;
        this.photoLink = photoLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", city='" + city + '\'' +
                ", birthday=" + birthday +
                ", photoLink='" + photoLink + '\'' +
                '}';
    }
}
