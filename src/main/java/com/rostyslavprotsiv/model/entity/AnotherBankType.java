package com.rostyslavprotsiv.model.entity;

public enum AnotherBankType {
    INTERNET_BANK, SIMPLE_BANK, WEB_WALLET
}
