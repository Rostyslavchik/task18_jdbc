package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

@Table(name = "another_bank")
public class AnotherBankEntity {
    @PrimaryKey
    @Column(name = "tov", length = 32)
    private String tov;
    @Column(name = "another_bank_info_name", length = 32)
    private String anotherBankInfoName;

    public AnotherBankEntity() {
    }

    public AnotherBankEntity(String tov, String anotherBankInfoName) {
        this.tov = tov;
        this.anotherBankInfoName = anotherBankInfoName;
    }

    public String getTov() {
        return tov;
    }

    public void setTov(String tov) {
        this.tov = tov;
    }

    public String getAnotherBankInfoName() {
        return anotherBankInfoName;
    }

    public void setAnotherBankInfoName(String anotherBankInfoName) {
        this.anotherBankInfoName = anotherBankInfoName;
    }

    @Override
    public String toString() {
        return "AnotherBankEntity{" +
                "tov='" + tov + '\'' +
                ", anotherBankInfoName='" + anotherBankInfoName + '\'' +
                '}';
    }
}
