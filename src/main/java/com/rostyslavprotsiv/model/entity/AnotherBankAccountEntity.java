package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

@Table(name = "another_bank_account")
public class AnotherBankAccountEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "type")
    private AnotherBankType type;
    @Column(name = "another_bank_tov", length = 32)
    private String anotherBankTov;

    public AnotherBankAccountEntity() {
    }

    public AnotherBankAccountEntity(int id, AnotherBankType type, String anotherBankTov) {
        this.id = id;
        this.type = type;
        this.anotherBankTov = anotherBankTov;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AnotherBankType getType() {
        return type;
    }

    public void setType(AnotherBankType type) {
        this.type = type;
    }

    public String getAnotherBankTov() {
        return anotherBankTov;
    }

    public void setAnotherBankTov(String anotherBankTov) {
        this.anotherBankTov = anotherBankTov;
    }

    @Override
    public String toString() {
        return "AnotherBankAccountEntity{" +
                "id=" + id +
                ", type=" + type +
                ", anotherBankTov='" + anotherBankTov + '\'' +
                '}';
    }
}
