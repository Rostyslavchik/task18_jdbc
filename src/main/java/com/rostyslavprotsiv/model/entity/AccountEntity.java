package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotation.Column;
import com.rostyslavprotsiv.model.entity.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.entity.annotation.Table;

import java.math.BigDecimal;

@Table(name = "account")
public class AccountEntity {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "dollar_amount")
    private BigDecimal dollarAmount;
    @Column(name = "type")
    private AccountType type;
    @Column(name = "client_id")
    private int clientId;

    public AccountEntity() {
    }

    public AccountEntity(int id, BigDecimal dollarAmount,
                         AccountType type, int clientId) {
        this.id = id;
        this.dollarAmount = dollarAmount;
        this.type = type;
        this.clientId = clientId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getDollarAmount() {
        return dollarAmount;
    }

    public void setDollarAmount(BigDecimal dollarAmount) {
        this.dollarAmount = dollarAmount;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", dollarAmount=" + dollarAmount +
                ", type=" + type +
                ", clientId=" + clientId +
                '}';
    }
}
