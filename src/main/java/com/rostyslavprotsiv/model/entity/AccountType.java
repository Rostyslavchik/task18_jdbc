package com.rostyslavprotsiv.model.entity;

public enum AccountType {
    CREDIT, SIMPLE, BONUS
}
