package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.AnotherBankEntity;

public interface IAnotherBankDAO extends IGeneralDAO<AnotherBankEntity, String>{
}
