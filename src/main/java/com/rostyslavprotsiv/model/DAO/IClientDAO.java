package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.ClientEntity;

public interface IClientDAO extends IGeneralDAO<ClientEntity, Integer>{
}
