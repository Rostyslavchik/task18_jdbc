package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.AnotherBankInfoEntity;

public interface IAnotherBankInfoDAO extends IGeneralDAO<AnotherBankInfoEntity,
        String>{
}
