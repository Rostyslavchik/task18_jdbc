package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.AccountEntity;

public interface IAccountDAO extends IGeneralDAO<AccountEntity, Integer>{
}
