package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.OperationEntity;

public interface IOperationDAO extends IGeneralDAO<OperationEntity, Integer>{
}
