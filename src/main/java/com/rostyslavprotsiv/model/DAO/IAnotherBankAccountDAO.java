package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.AnotherBankAccountEntity;

public interface IAnotherBankAccountDAO extends IGeneralDAO<AnotherBankAccountEntity, Integer>{
}
